# Quick start with gRPC Java

This is a quick-start guide to gRPC with Java based on [Java Quick Start](https://grpc.io/docs/quickstart/java/)

## Requirements

This project is built with Java 11 (OpenJDK 11) and Gradle 5. Those tools must be installed in order to be build this project.

## Initialise the project

To initialise the project execute following command in the project directory:

    gradle init

Accept all default selections (type - basic, DSL - Groovy).

This will create build scripts and Gradle wrapper, which should be used to build and run the project.

Create the directory for Java source code:

    mkdir -p src/main/java

Create the directory for Proto Buffer definition files (`.proto`):

    mkdir -p src/main/proto

## Add gRPC dependencies

Next need to add gRPC dependencies and plugins setup to gradle build.
Add following to the `build.gradle` file:

    apply plugin: "application"
    apply plugin: "com.google.protobuf"
    apply plugin: "idea"

    buildscript {
        repositories {
            mavenCentral()
        }
        dependencies {
            classpath "com.google.protobuf:protobuf-gradle-plugin:0.8.10"
        }
    }

    group = "org.quickstart.java.grpc"
    version = "1.0-SNAPSHOT"

    sourceCompatibility = 1.8

    repositories {
        mavenCentral()
    }

    dependencies {
        implementation "io.grpc:grpc-netty-shaded:1.22.1"
        implementation "io.grpc:grpc-protobuf:1.22.1"
        implementation "io.grpc:grpc-stub:1.22.1"
        compileOnly "javax.annotation:javax.annotation-api:1.2"
    }

    protobuf {
        protoc {
            artifact = "com.google.protobuf:protoc:3.7.1"
        }
        plugins {
            grpc {
                artifact = 'io.grpc:protoc-gen-grpc-java:1.22.1'
            }
        }
        generateProtoTasks {
            all()*.plugins {
                grpc {}
            }
        }
    }

    // Inform IDEs like IntelliJ IDEA, Eclipse or NetBeans about the generated code.
    sourceSets {
        main {
            java {
                srcDirs 'build/generated/source/proto/main/grpc'
                srcDirs 'build/generated/source/proto/main/java'
            }
        }
    }

Run the build to make sure that build configuration is correct and to download project dependencies:

    ./gradlew build

## Define Proto Buffer file and compile stubs

To `src/main/proto` directory add file `helloworld.proto`:

    syntax = "proto3";

    option java_package = "org.quickstart.java.grpc.helloworld";
    option java_multiple_files = true;

    package helloworld;

    service Greeter {
        rpc SayHello (HelloRequest) returns (HelloReply) {}
    }

    message HelloRequest {
        string name = 1;
    }

    message HelloReply {
        string message = 1;
    }

Build the project:

    ./gradlew build

This should generated stubs and gRPC classes into `build/generated/source/proto/main/grpc` and `build/generated/source/proto/main/java` directories.

## Create Server

Create package e.g. `org.quickstart.java.grpc.helloworld` in `src/main/java` directory and in this package
create new Java Class `HelloWorldServer`:

    package org.quickstart.java.grpc.helloworld;

    public class HelloWorldServer {
    }

Define `server` variable of type `io.grpc.Server`and define methods to stop, start and await for termination of this server.
Define `main` method to run this Server:

    package org.quickstart.java.grpc.helloworld;
    
    import io.grpc.Server;
    import io.grpc.ServerBuilder;
    
    import java.io.IOException;
    
    public class HelloWorldServer {
        // gRPC server
        private Server server;
    
        public static void main(String[] args) throws IOException, InterruptedException {
            final HelloWorldServer server = new HelloWorldServer();
            server.start();
            server.blockUntilShutdown();
        }

        // start gRPC server    
        private void start() throws IOException {
            int port = 50051; // The port on which the server should run
            server = ServerBuilder.forPort(port)
                .build()
                .start();
    
            // add shutdown hook to gracely shutdown gRPC server on program exit
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                HelloWorldServer.this.stop();
                System.err.println("*** server shut down");
            }));
        }
    
        // stop gRPC server
        private void stop() {
            if (server != null) {
                server.shutdown();
            }
        }
    
        private void blockUntilShutdown() throws InterruptedException {
            if (server != null) {
                server.awaitTermination();
            }
        }
    }

Build the project:

    ./gradlew clean build
    
To be able to run the project add the following to the end of `build.gradle` file:

    startScripts.enabled = false
    
    task helloWorldServer(type: CreateStartScripts) {
        mainClassName = 'org.quickstart.java.grpc.helloworld.HelloWorldServer'
        applicationName = 'hello-world-server'
        outputDir = new File(project.buildDir, 'tmp')
        classpath = startScripts.classpath
    }
    
    task helloWorldClient(type: CreateStartScripts) {
        mainClassName = 'org.quickstart.java.grpc.helloworld.HelloWorldClient'
        applicationName = 'hello-world-client'
        outputDir = new File(project.buildDir, 'tmp')
        classpath = startScripts.classpath
    }
    
    applicationDistribution.into('bin') {
        from(helloWorldServer)
        from(helloWorldClient)
        fileMode = 0755
    }

This will create start scripts for both server and client. Build start script for server with the following command:

    ./gradlew installDist

Run the build server with the following command:

    ./build/install/grpc-java/bin/hello-world-server

To stop the server press Ctrl+C, command will print termination statements and will exit.

## Create Client

In the package `org.quickstart.java.grpc.helloworld` in `src/main/java`
create new Java Class `HelloWorldClient`. Define channel, constructors, helper methods and main method to run it.

    package org.quickstart.java.grpc.helloworld;
    
    import io.grpc.ManagedChannel;
    import io.grpc.ManagedChannelBuilder;
    
    import java.util.concurrent.TimeUnit;
    
    public class HelloWorldClient {
        private final ManagedChannel channel;
    
        private HelloWorldClient(String host, int port) {
            this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext()
                .build());
        }
    
        private HelloWorldClient(ManagedChannel channel) {
            this.channel = channel;
        }
    
        private void shutdown() throws InterruptedException {
            System.out.println("Shutting down channel");
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
    
        public static void main(String[] args) throws InterruptedException {
            HelloWorldClient client = new HelloWorldClient("localhost", 50051);
            client.shutdown();
        }
    }

Rebuild the project

    ./gradlew installDist
    
and run the client start script:

    ./build/install/grpc-java/bin/hello-world-client
    
Program will do nothing except print termination message.

## Add Logic to execute RPC

Currently the server and client does not do anything and not even communicating with each other.
Now it's time to add logic to actually execute RPC. 

### Implement gRPC service

For that need to implement gRPC service.
Add the following code to the end of `HelloWorldServer` class (not this new class is inner class):

    // Implementation of GreeterGrpc service defined in proto file
    static class GreeterImpl extends GreeterGrpc.GreeterImplBase {
        // it needs to override all method defined for this server
        @Override
        public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
            // build reply
            HelloReply reply = HelloReply.newBuilder().setMessage("Hello " + request.getName()).build();
            // sends reply
            responseObserver.onNext(reply);
            // notify client about completion
            responseObserver.onCompleted();
        }
    }
 
Register this service into gRPC server by adding the following code `.addService(new GreeterImpl())`
to place where gRPC server is build, like following:

    server = ServerBuilder.forPort(port)
        // register gRPC service implementation
        .addService(new GreeterImpl())
        .build()
        .start();

### Implement gRPC client

Now need to add stub calls to the client so it will call registered service.
At the beginning of `HelloWorldClient` class add stub variable after `channel` variable:

    private final GreeterGrpc.GreeterBlockingStub blockingStub;
    
Add stub initialisation code to second constructor:

    private HelloWorldClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = GreeterGrpc.newBlockingStub(channel);
    }

Add `greet` method which will send the message to the gRPC service:

    private void greet(String name) {
        System.out.println("Will try to greet " + name + " ...");
        // build request object
        HelloRequest request = HelloRequest.newBuilder().setName(name).build();
        HelloReply response;
        try {
            // execute RPC and try to get response
            response = blockingStub.sayHello(request);
        } catch (StatusRuntimeException ex) {
            System.out.println(String.format("RPC failed: %s", ex.getStatus()));
            return;
        }
        System.out.println("Greeting: " + response.getMessage());
    }

Add code to `main` method to use this `greet` method:

    public static void main(String[] args) throws InterruptedException {
        HelloWorldClient client = new HelloWorldClient("localhost", 50051);
        try {
            String user = "world";
            if (args.length > 0) {
                user = args[0];
            }
            // call the greet method with the user name
            client.greet(user);
        } finally {
            // finally shutdown the client
            client.shutdown();
        }
    }

Build distribution:

    ./gradlew installDist

Start the server in one terminal:

    ./build/install/grpc-java/bin/hello-world-server
    
Run the client in the other terminal:

    ./build/install/grpc-java/bin/hello-world-client
    
In the client terminal should see output like this:

    Will try to greet world ...
    Greeting: Hello world
    Shutting down channel

## Update a gRPC service

Let’s update this so that the `Greeter` service has two methods. Edit `src/main/proto/helloworld.proto`
and update it with a new `SayHelloAgain` method, with the same request and response types:

    service Greeter {
        rpc SayHello (HelloRequest) returns (HelloReply) {}
        rpc SayHelloAgain (HelloRequest) returns (HelloReply) {}
    }

Rebuild project to regenerate gRPC service with new method.
Now need to implement this method on the server and call it from the client.

### Update the server

In `HelloWorldServer.GreeterImpl` class implement the new method like this:

    @Override
    public void sayHelloAgain(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        HelloReply reply = HelloReply.newBuilder().setMessage("Hello again " + request.getName()).build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

### Update the client

In the same class `HelloWorldClient` call the new method like this:

    try {
        response = blockingStub.sayHelloAgain(request);
    } catch (StatusRuntimeException ex) {
        System.out.println(String.format("RPC failed: %s", ex.getStatus()));
        return;
    }
    System.out.println("Greeting: " + response.getMessage());

It can be added at the end of the `greet` method:

    private void greet(String name) {
        System.out.println("Will try to greet " + name + " ...");
        HelloRequest request = HelloRequest.newBuilder().setName(name).build();
        HelloReply response;
        try {
            response = blockingStub.sayHello(request);
        } catch (StatusRuntimeException ex) {
            System.out.println(String.format("RPC failed: %s", ex.getStatus()));
            return;
        }
        System.out.println("Greeting: " + response.getMessage());

        try {
            response = blockingStub.sayHelloAgain(request);
        } catch (StatusRuntimeException ex) {
            System.out.println(String.format("RPC failed: %s", ex.getStatus()));
            return;
        }
        System.out.println("Greeting: " + response.getMessage());
    }

Compile the client and server

    ./gradlew installDist

Run the server

    ./build/install/examples/bin/hello-world-server

In another terminal, run the client

    ./build/install/examples/bin/hello-world-client

This concludes Quick-Start guide for gRPC with Java.
